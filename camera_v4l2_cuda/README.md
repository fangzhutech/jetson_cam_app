# camera_v4l2_cuda 
通过CUDA 函数处理V4L2数据减低CPU消耗 (含get camera capture timestamp)
前提Jetson上必须已安装jetson_multimedia_api，或直接去目录下编译运行：
/usr/src/jetson_multimedia_api/samples/12_camera_v4l2_cuda

编译：
make 

命令：
./camera_v4l2_cuda -d /dev/video0 -s 1920x1080 -f UYVY -n 30 -c 

不同相机可能需要修改分辨率和YUV顺序

