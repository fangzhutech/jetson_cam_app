# jetson_cam_app

#### Description
Jetson camera applications
 (Based on IMX390/GW5200 1920x1080@30fps GMSL2 Camera )

#### V4L2 CUDA Camera Demo
./camera_v4l2_cuda -d /dev/video0 -s 1920x1080 -f UYVY -n 30 -c 

#### V4L2 OpenCV Camera Demo
./camera_v4l2_opencv -d /dev/video0 -s 1920x1080 -f UYVY -n 30 -c 


#### GStreamer Shell
format=UYVY/YUY2/VYUY

Preview YUV Camera
gst-launch-1.0 -ev v4l2src device=/dev/video0 ! 'video/x-raw,format=UYVY,width=1920,height=1080' ! videoconvert ! fpsdisplaysink video-sink=xvimagesink sync=false

Check YUV fps (no preview display)
gst-launch-1.0 -ev v4l2src device=/dev/video0 ! "video/x-raw, format=(string)UYVY, width=(int)1920, height=(int)1080" ! fpsdisplaysink text-overlay=0 video-sink=fakesink sync=0

V4L2 Capture (yuv player to check dat.yuv)
v4l2-ctl --set-fmt-video=width=1920,height=1080,pixelformat=UYVY --set-ctrl bypass_mode=0 --stream-mmap --stream-count=60 --stream-to=dat.yuv -d /dev/video0 

format=RAW 
Preview RAW Camera
gst-launch-1.0 nvarguscamerasrc sensor-id=0 ! 'video/x-raw(memory:NVMM),width=1920,height=1080,framerate=30/1,format=NV12' ! nvvidconv ! fpsdisplaysink video-sink=xvimagesink sync=false

Check RAW fps (no preview display)
gst-launch-1.0 nvarguscamerasrc sensor-id=0 ! 'video/x-raw(memory:NVMM),width=1920,height=1080,framerate=30/1,format=NV12' ! nvvidconv ! fpsdisplaysink video-sink=xvimagesink sync=false 


#### Verified gmsl cameras list

8M Camera -> 3840x2160@30fps
AR0820 + GW5400 + MAX9295A
OX08BC + GW5300 + MAX9295A

2M Camera -> 1920x1080@30fps
AR0231 + AP0200|AP0202 + MAX9295A|MAX96705
IMX390 + GW5200 + MAX9295A|MAX96717F
AR0233 + GW5200 + MAX9295A
OX03CC + GW5200 + MAX9295A
OX03CC + GW5200 + MAX96717F

1M Camera -> 1280x720@30fps
AR0147 + AP0101 + MAX96705
OX01FC + MAX96705|MAX96715
