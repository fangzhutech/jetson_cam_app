# jetson_cam_app

### 介绍
Jetson camera applications
（默认 IMX390/GW5200 1920x1080@30fps GMSL2相机 ）

### V4L2 CUDA Camera Demo 
通过CUDA 函数处理V4L2数据减低CPU消耗 (含get camera capture timestamp)
前提Jetson上必须已安装jetson_multimedia_api，或直接去目录下编译运行：
/usr/src/jetson_multimedia_api/samples/12_camera_v4l2_cuda

编译：
make 

命令：
./camera_v4l2_cuda -d /dev/video0 -s 1920x1080 -f UYVY -n 30 -c 

（不同相机可能需要修改分辨率和YUV顺序）

### V4L2 OpenCV Camera Demo
V4L2 例程，并通过OpenCV 方式显示，代码含get camera capture timestamp

编译：
make 

命令：
./camera_v4l2_opencv -d /dev/video0 -s 1920x1080 -f UYVY -n 30 -c 

（不同相机可能需要修改分辨率和YUV顺序）

### GStreamer 命令


#### 预览YUV 相机 - format=UYVY/YUY2/VYUY

gst-launch-1.0 -ev v4l2src device=/dev/video0 ! 'video/x-raw,format=UYVY,width=1920,height=1080' ! videoconvert ! fpsdisplaysink video-sink=xvimagesink sync=false

查看YUV 相机帧率（不显示画面）

gst-launch-1.0 -ev v4l2src device=/dev/video0 ! "video/x-raw, format=(string)UYVY, width=(int)1920, height=(int)1080" ! fpsdisplaysink text-overlay=0 video-sink=fakesink sync=0

V4L2 保存数据帧（可通过yuv player 查看dat.yuv ）

v4l2-ctl --set-fmt-video=width=1920,height=1080,pixelformat=UYVY --set-ctrl bypass_mode=0 --stream-mmap --stream-count=60 --stream-to=dat.yuv -d /dev/video0 


#### 预览RAW 相机 - format=RAW 

gst-launch-1.0 nvarguscamerasrc sensor-id=0 ! 'video/x-raw(memory:NVMM),width=1920,height=1080,framerate=30/1,format=NV12' ! nvvidconv ! fpsdisplaysink video-sink=xvimagesink sync=false

查看RAW 相机帧率（不显示画面）

gst-launch-1.0 nvarguscamerasrc sensor-id=0 ! 'video/x-raw(memory:NVMM),width=1920,height=1080,framerate=30/1,format=NV12' ! nvvidconv ! fpsdisplaysink video-sink=xvimagesink sync=false 

### 目前已验证过的相机包括但不限于

8M 相机 -> 3840x2160@30fps

AR0820 + GW5400 + MAX9295A

OX08BC + GW5300 + MAX9295A


2M 相机 -> 1920x1080@30fps

AR0231 + AP0200|AP0202 + MAX9295A|MAX96705

IMX390 + GW5200 + MAX9295A|MAX96717F

AR0233 + GW5200 + MAX9295A

OX03CC + GW5200 + MAX9295A

OX03CC + GW5200 + MAX96717F


1M 相机 -> 1280x720@30fps

AR0147 + AP0101 + MAX96705

OX01FC + MAX96705|MAX96715

